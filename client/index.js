'use strict';
(async () => {
    const conninfo = await fetch('wsinfo', {
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
         }
  
      })
    .then(response => response.json());
  const wsConnString = conninfo.port === "" ? `wss://${conninfo.domain}` : `wss://${conninfo.domain}:${conninfo.port}`
  const exampleSocket = new WebSocket(wsConnString);
  let localuser;

  exampleSocket.addEventListener('open', function (event) {
    localuser = prompt('Who Are You?');

    exampleSocket.send(
      JSON.stringify(
        {
          messageType: 'newUser',
          data: localuser
        }
      )
    );
  });

  exampleSocket.addEventListener('message', function (event) {
    const receivedBroadcast = JSON.parse(event.data)
    if (receivedBroadcast['messageType'] === 'userList') {
      refreshUsers(receivedBroadcast.data);
    } else { // 'chatMessage'
      appendChat(receivedBroadcast.data);
    }
  });

  function refreshUsers(userList) {
    const listNode = document.querySelector('#user-list');
    listNode.innerHTML = '';

    userList.forEach(user => {
      const userNode = document.createElement('p');
      userNode.innerHTML = user + (user === localuser ? ' (you)' : '');

      listNode.appendChild(userNode);
    });
  }

  function appendChat(chatObject) {
    const chatNode = document.createElement('p');
    chatNode.innerHTML = `<b>${chatObject.user}:&nbsp</b>${chatObject.text}`;

    document.querySelector('#chat-section').appendChild(chatNode);
  }

  function sendChat(text) {
    exampleSocket.send(
      JSON.stringify(
        {
          messageType: 'chatMessage',
          data: {
            user: localuser,
            text
          }
        }
      )
    );
  }

  document.querySelector("#text-input").addEventListener('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {
      sendChat(e.target.value);
      e.target.value = '';
    }
  });

})();