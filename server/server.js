'use strict';
const WebSocket = require('ws');
const http = require('http');
const fs = require('fs');
var path = require('path');

const CONFIG = {
    wsconninfo: '{"domain": "itpiesummit.chrispilcher.me", "port": "8080"}'
}

const wsServer = new WebSocket.Server({ port: 8080 });
wsServer.on('connection', function connection(socket) {
    console.log("received ws connection");
  socket.on('message', function incoming(message) {
    const receivedMessage = JSON.parse(message);
    let broadCast = {};
    console.log("received message");

    if (receivedMessage['messageType'] === 'newUser') {
      socket.username = receivedMessage.data;

      broadCast = {
        messageType: 'userList',
        data: getUserList(wsServer)
      };

    } else {
      // broadcast incoming chat message as-is
      broadCast = receivedMessage;
    }

    getActiveClients(wsServer).forEach(client => {
      client.send(JSON.stringify(broadCast));
    });
  });

  socket.on('close', function() {
    getActiveClients(wsServer).forEach(client => {
      client.send(
        JSON.stringify(
          {
            messageType: 'userList',
            data: getUserList(wsServer)
          }
        )
      );
    });
  });

});

function getActiveClients(server) {
  return [...server.clients].filter(c => c.readyState === WebSocket.OPEN);
}

function getUserList(server) {
  return getActiveClients(server).map(c => c.username)
}

http.createServer((request, response) => {
    if (request.url === "/wsinfo") {
        const contentType = 'application/json';
        response.writeHead(200, {'Content-Type': contentType});
        response.end(CONFIG.wsconninfo);
    }
  console.log('request ', request.url);
  let filePath = './client'+request.url;
  if (filePath === './client/') {
    filePath = './client/index.html';
  }
  fs.readFile(filePath, function(error, content) {
    if (error) {
      if(error.code == 'ENOENT') {
          response.writeHead(404, { 'Content-Type': 'text/html' });
          response.end(content, 'utf-8');
      }
      else {
        response.writeHead(500);
        response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
      }
    } else {
      let extname = String(path.extname(filePath)).toLowerCase();
      var mimeTypes = {
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.css': 'text/css',
        '.json': 'application/json',
        '.png': 'image/png',
        '.jpg': 'image/jpg',
        '.gif': 'image/gif',
        '.svg': 'image/svg+xml',
        '.wav': 'audio/wav',
        '.mp4': 'video/mp4',
        '.woff': 'application/font-woff',
        '.ttf': 'application/font-ttf',
        '.eot': 'application/vnd.ms-fontobject',
        '.otf': 'application/font-otf',
        '.wasm': 'application/wasm'
      };

      let contentType = mimeTypes[extname] || 'application/octet-stream';
      response.writeHead(200, { 'Content-Type': contentType });
      response.end(content, 'utf-8');
    }
  });


}).listen(8000)

/* 
  interface Message
  {
    messageType: 'newUser' | 'chatMessage' | 'userList';
    data: string | string[] | Chat;
  }

  interface Chat
  {
    user: string;
    text: string;
  }
*/
