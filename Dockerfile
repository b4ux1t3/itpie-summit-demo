FROM node:latest

WORKDIR /app
COPY . .

EXPOSE 8000
EXPOSE 8080

RUN npm install

ENTRYPOINT [ "npm", "start" ]